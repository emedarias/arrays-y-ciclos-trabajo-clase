//llamamos al boton "submit" y lo guardamos en la variable boton
let boton = document.querySelector("input[type='submit']");
//Le asignamos al boton la funcion "validar"
boton.addEventListener("click", validar);

function validar(event)
	{
		//Evitamos que el formulario se envie
		event.preventDefault();
		//Guardamos todos los input[type="radio"] en un Array llamado radios.
		let radios = Array.from(document.querySelectorAll("input[type='radio']"));
		//Guardamos todos los input[type="checkbox"] en un Array llamado checks.
		let checks = Array.from(document.querySelectorAll("input[type='checkbox']"));
		//llamamos al campo donde vamos a mostrar los checks y radios seleccionados.
		let elegidos = document.querySelector("#mostrar");
		//Limpiamos el parrafo donde se van a imprimir todos los checks y radios.
		elegidos.innerHTML="";
		//Ciclamos por el Array radios.
		for(elemento of radios)
			{
				//Si el elemento esta tildado...
				if(elemento.checked)
					{
						//Agregamos el dato al campo donde se va a mostrar.
						elegidos.innerHTML+=elemento.value+", ";
					}
			}
		//Ciclamos por el Array checks.
		for(elemento of checks)
			{
				//Si el elemento esta tildado...
				if(elemento.checked)
					{
						//Agregamos el dato al campo donde se va a mostrar.
						elegidos.innerHTML+=elemento.value+", ";
					}	
			}
	}